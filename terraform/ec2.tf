# Control node
resource "aws_instance" "master" {
  count         = 1
  ami           = var.image
  instance_type = var.instance
  //subnet_id       = aws_subnet.dmz.id
  user_data = file("postinstall-m.sh")
  #private_ip    = "10.0.1.69"
  vpc_security_group_ids = [aws_security_group.allow_http_ssh.id]

  tags = {
    Owner = var.owner
    Name  = "master${count.index + 1}"
  }
}

# Workers nodes
resource "aws_instance" "worker" {
  count         = 3
  ami           = var.image
  instance_type = var.instance
  //subnet_id       = aws_subnet.dmz.id
  #user_data       = file("postinstall.sh")
  vpc_security_group_ids = [aws_security_group.allow_http_ssh.id]
  user_data              = file("postinstall-w.sh")

  tags = {
    Owner = var.owner
    Name  = "worker${count.index + 1}"
  }
}

