#!/bin/bash

apt update

apt install -y nginx

echo "Welcome to Terraform created web server" > /var/www/html/index.html

# Create User
useradd -s /bin/bash -c "tux" -m tux
echo "Passw0rd" | passwd --stdin tux
# Set sudo
echo "tux ALL=(ALL)       NOPASSWD: ALL" >> /etc/sudoers
# Deploy SSH keys
mkdir /home/tux/.ssh
echo "ssh-ed25519 xxxx" >> /home/tux/.ssh/authorized_keys
# Set proper permissions
chown -R tux:tux /home/tux/.ssh
chmod 700 /home/tux/.ssh
chmod 600 /home/tux/.ssh/authorized_keys
##

# install k3s
#curl -sfL https://get.k3s.io | sh -

#apt-get install -y apt-transport-https ca-certificates curl
#curl -fsSLo /etc/apt/keyrings/kubernetes-archive-keyring.gpg https://packages.cloud.google.com/apt/doc/apt-key.gpg
#echo "deb [signed-by=/etc/apt/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list
#apt-get update
#apt-get install -y kubelet kubeadm kubectl
#apt-mark hold kubelet kubeadm kubectl
