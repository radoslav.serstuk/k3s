#!/bin/bash
# This scripts performs postinstall configuraton on deployed VMs.

# Variables
LBIP=`hostname -I | awk '{print $1}'`

# Install packages
apt -y install git

# Create User
useradd -s /bin/bash -c "Admin" -m tux
echo "Passw0rd" | passwd --stdin tux

# Set sudo
echo "tux ALL=(ALL)       NOPASSWD: ALL" >> /etc/sudoers

# Deploy SSH keys
mkdir /home/tux/.ssh
cat <<EOF | tee -a /home/tux/.ssh/authorized_keys
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCedYC+haUHPXidZ4L2ganr0+EB8g89dL/K24Twbj2CR868MMB5XPv0evLTqVDRC3iiyP647chxxuMv3UyU6/IVxuI6GpdyEqAYgAOYSgHVkd785i58O0fzSeLHB+aEtmQQPJYGKXjh4KCyNz3cSgDuYftHXRflhigVhiwE76yrJiTcv01Fp9AbDF9lWFKqnrAIBlgfFy3u/z//7ewkRzRH1ZAE7wptoCuc76d85sU0t/xdskdYPQ/upC90nt69GmhB7jR2qiXwVDiMB6SFhOWrGgI8WrmmYSrhu0CzvLQv9WYi93N/FMqbXg47vWr8TncrBWLj/JkuKEwhwAxUheMH rado
EOF
# Set proper permissions
chown -R tux /home/tux/.ssh
chmod 700 /home/tux/.ssh
chmod 600 /home/tux/.ssh/authorized_keys
##

## Init K3S
curl -sfL https://get.k3s.io | sh -
systemctl stop k3s
#

# Join to the Cluster
sleep 4m
masterip=10.0.1.69
wget http://${masterip}:8080/token 
token=`cat token`
k3s agent --server https://${masterip}:6443 --token ${token}

