output "instance_public_ip" {
  description = "Public IP of EC2 instance"
  value       = aws_instance.master[0].public_ip
}
/*
output "instance_public_ip_worker" {
  description = "Public IP of EC2 instance"
  value       = aws_instance.worker*.public_ip
}
*/
